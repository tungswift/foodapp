/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author tungthai
 */
public class MonAnDonHang {
    private int maDonHang;
    private int maMonAn;
    private int soLuong;

    public MonAnDonHang() {
    }

    public MonAnDonHang(int maDonHang, int maMonAn, int soLuong) {
        this.maDonHang = maDonHang;
        this.maMonAn = maMonAn;
        this.soLuong = soLuong;
    }

    public int getMaDonHang() {
        return maDonHang;
    }

    public void setMaDonHang(int maDonHang) {
        this.maDonHang = maDonHang;
    }

    public int getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(int maMonAn) {
        this.maMonAn = maMonAn;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }
    
}
