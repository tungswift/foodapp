/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author tungthai
 */
public class CuaHang {
    private int maCuaHang;
    private String tenCuaHang, diaChi, logo;
    private int thoiGianMoCua, thoiGianDongCua;
    private int tongDiem;
    private int soLuotCham;
    private double lat, lng;

    public CuaHang() {
    }

    public CuaHang(int maCuaHang, String tenCuaHang, String diaChi, String logo, int thoiGianMoCua, int thoiGianDongCua, int tongDiem, int soLuotCham, double lat, double lng) {
        this.maCuaHang = maCuaHang;
        this.tenCuaHang = tenCuaHang;
        this.diaChi = diaChi;
        this.logo = logo;
        this.thoiGianMoCua = thoiGianMoCua;
        this.thoiGianDongCua = thoiGianDongCua;
        this.tongDiem = tongDiem;
        this.soLuotCham = soLuotCham;
        this.lat = lat;
        this.lng = lng;
    }

    

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    

    public int getMaCuaHang() {
        return maCuaHang;
    }

    public void setMaCuaHang(int maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    public String getTenCuaHang() {
        return tenCuaHang;
    }

    public void setTenCuaHang(String tenCuaHang) {
        this.tenCuaHang = tenCuaHang;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getTongDiem() {
        return tongDiem;
    }

    public void setTongDiem(int tongDiem) {
        this.tongDiem = tongDiem;
    }

    public int getSoLuotCham() {
        return soLuotCham;
    }

    public void setSoLuotCham(int soLuotCham) {
        this.soLuotCham = soLuotCham;
    }

    

    public int getThoiGianMoCua() {
        return thoiGianMoCua;
    }

    public void setThoiGianMoCua(int thoiGianMoCua) {
        this.thoiGianMoCua = thoiGianMoCua;
    }

    public int getThoiGianDongCua() {
        return thoiGianDongCua;
    }

    public void setThoiGianDongCua(int thoiGianDongCua) {
        this.thoiGianDongCua = thoiGianDongCua;
    }


    
    
}
