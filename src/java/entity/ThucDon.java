/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author tungthai
 */
public class ThucDon {
    private int maCuaHang, maMonAn;
    private String tenMonAn;
    private int soLanDat, donGia;
    private String anhMinhHoa;

    public ThucDon() {
    }

    public ThucDon(int maCuaHang, int maMonAn, String tenMonAn, int soLanDat, int donGia, String anhMinhHoa) {
        this.maCuaHang = maCuaHang;
        this.maMonAn = maMonAn;
        this.tenMonAn = tenMonAn;
        this.soLanDat = soLanDat;
        this.donGia = donGia;
        this.anhMinhHoa = anhMinhHoa;
    }

    public int getMaCuaHang() {
        return maCuaHang;
    }

    public void setMaCuaHang(int maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    public int getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(int maMonAn) {
        this.maMonAn = maMonAn;
    }

    public String getTenMonAn() {
        return tenMonAn;
    }

    public void setTenMonAn(String tenMonAn) {
        this.tenMonAn = tenMonAn;
    }

    public int getSoLanDat() {
        return soLanDat;
    }

    public void setSoLanDat(int soLanDat) {
        this.soLanDat = soLanDat;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public String getAnhMinhHoa() {
        return anhMinhHoa;
    }

    public void setAnhMinhHoa(String anhMinhHoa) {
        this.anhMinhHoa = anhMinhHoa;
    }
    
    
}
