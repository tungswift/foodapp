/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Date;

/**
 *
 * @author tungthai
 */
public class DonHang {
    private int maDonHang;
    private int tongTien;
    private String trangThaiDonHang;
    private Date ngayDat;
    private int maTaiKhoan;
    private int maCuaHang;
    
    private String ngayGiaoHang;
    private String gioGiaoHang;
    private String diaChiGiaoHang;
    private String soDienThoai;
    private String ghiChu;

    public DonHang() {
    }

    public DonHang(int maDonHang, int tongTien, String trangThaiDonHang, Date ngayDat, int maTaiKhoan, int maCuaHang) {
        this.maDonHang = maDonHang;
        this.tongTien = tongTien;
        this.trangThaiDonHang = trangThaiDonHang;
        this.ngayDat = ngayDat;
        this.maTaiKhoan = maTaiKhoan;
        this.maCuaHang = maCuaHang;
    }
    
    public int getMaTaiKhoan() {
        return maTaiKhoan;
    }

    public void setMaTaiKhoan(int maTaiKhoan) {
        this.maTaiKhoan = maTaiKhoan;
    }

    public int getMaCuaHang() {
        return maCuaHang;
    }

    public void setMaCuaHang(int maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    

    public int getMaDonHang() {
        return maDonHang;
    }

    public void setMaDonHang(int maDonHang) {
        this.maDonHang = maDonHang;
    }

    public int getTongTien() {
        return tongTien;
    }

    public void setTongTien(int tongTien) {
        this.tongTien = tongTien;
    }

    public String getTrangThaiDonHang() {
        return trangThaiDonHang;
    }

    public void setTrangThaiDonHang(String trangThaiDonHang) {
        this.trangThaiDonHang = trangThaiDonHang;
    }

    public Date getNgayDat() {
        return ngayDat;
    }

    public void setNgayDat(Date ngayDat) {
        this.ngayDat = ngayDat;
    }

    public String getNgayGiaoHang() {
        return ngayGiaoHang;
    }

    public void setNgayGiaoHang(String ngayGiaoHang) {
        this.ngayGiaoHang = ngayGiaoHang;
    }

    public String getGioGiaoHang() {
        return gioGiaoHang;
    }

    public void setGioGiaoHang(String gioGiaoHang) {
        this.gioGiaoHang = gioGiaoHang;
    }

    public String getDiaChiGiaoHang() {
        return diaChiGiaoHang;
    }

    public void setDiaChiGiaoHang(String diaChiGiaoHang) {
        this.diaChiGiaoHang = diaChiGiaoHang;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    
    
}
