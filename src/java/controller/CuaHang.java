/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CuaHangDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tungthai13
 */
public class CuaHang extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        //Kiểm tra loại cửa hàng
        String timKiem = request.getParameter("timKiem");
        String loaiCuaHangParam = request.getParameter("loaiCuaHang");
        String loaiCuaHang = null;
        if (loaiCuaHangParam.equalsIgnoreCase("1")) {
            loaiCuaHang = "Đồ uống";
        }
        if (loaiCuaHangParam.equalsIgnoreCase("2")) {
            loaiCuaHang = "Bánh";
        }
        if (loaiCuaHangParam.equalsIgnoreCase("3")) {
            loaiCuaHang = "Đồ ăn";
        }

        //Tinh toan tong so trang
        int tongSoCuaHang = new CuaHangDAO().tongSoCuaHangCungLoai(loaiCuaHang);
        int tongSoTrang = tongSoCuaHang / 9;
        if (tongSoTrang < 1) {
            tongSoTrang = 1;
        } else if (tongSoTrang % 9 != 0) {
            tongSoTrang += 1;
        }

        //Kiem tra dieu kien cua trang hien tai
        String trangHienTai = "";
        trangHienTai = request.getParameter("trangHienTai");
        if (trangHienTai == null) {
            trangHienTai = "1";
        } else {
            int trangHienTaiInteger = Integer.parseInt(trangHienTai);
            if (trangHienTaiInteger <= 0) {
                trangHienTai = "1";
            }
            if (trangHienTaiInteger > tongSoTrang) {
                trangHienTai = tongSoTrang + "";
            }
        }

        request.setAttribute("trangHienTai", trangHienTai);
        request.setAttribute("tongSoTrang", tongSoTrang);
        if (timKiem == null) {
            List<entity.CuaHang> list = new CuaHangDAO().tatCaCuaHangCungLoai(loaiCuaHang, Integer.parseInt(trangHienTai), 9);
            request.setAttribute("list", list);
        } else {
            List<entity.CuaHang> list = new CuaHangDAO().timKiemCuaHangCungLoai(timKiem, loaiCuaHang);
            request.setAttribute("list", list);
        }
        request.setAttribute("loaiCuaHang", loaiCuaHangParam);

        request.getRequestDispatcher("/cuahangcungloai.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
