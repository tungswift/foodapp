/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.MonAnChon;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tungthai
 */
public class ThanhToan extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        request.setCharacterEncoding("UTF-8");
        
        String diaChi = request.getParameter("diaChi");
        String ngay = request.getParameter("ngay");
        String gio = request.getParameter("gio");
        String ghiChu = request.getParameter("ghiChu");
        String tenCuaHang = request.getParameter("tenCuaHang");
        String diaChiCuaHang = request.getParameter("diaChiCuaHang");
        String json = request.getParameter("json");
        String soDT = request.getParameter("soDT");
        String maCuaHang = request.getParameter("maCuaHang");
        
        //Xu Ly JSON bang thu vien JACKSON 2.6.3
        ObjectMapper mapper = new ObjectMapper();
        List<MonAnChon> list = mapper.readValue(json, new TypeReference<List<MonAnChon>>(){});
        
        request.setAttribute("gioHang", list);
        request.setAttribute("tenCuaHang", tenCuaHang);
        request.setAttribute("diaChiCuaHang", diaChiCuaHang);
        request.setAttribute("soDT", soDT);
        request.setAttribute("diaChi", diaChi);
        request.setAttribute("ngay", ngay);
        request.setAttribute("gio", gio);
        request.setAttribute("ghiChu", ghiChu);
        request.setAttribute("maCuaHang", maCuaHang);
        
        request.getRequestDispatcher("/thanhtoan.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
