/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.CuaHangDAO;
import dao.TaiKhoanDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tungthai13
 */
@WebServlet(name = "ChamDiem", urlPatterns = {"/ChamDiem"})
public class ChamDiem extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String paramDiem = request.getParameter("diem");
        String paramMaCuaHang = request.getParameter("maCuaHang");
        String paramMaTaiKhoan = request.getParameter("maTaiKhoan");

        int diem = Integer.parseInt(paramDiem);
        int tongDiem = new CuaHangDAO().getTongDiemCuaHang(Integer.parseInt(paramMaCuaHang));
        int soLuotCham = new CuaHangDAO().getSoLuotCham(Integer.parseInt(paramMaCuaHang));

        int lichSuChamDiem = new TaiKhoanDAO().diemChamCuaHang(Integer.parseInt(paramMaCuaHang), Integer.parseInt(paramMaTaiKhoan));
        if (lichSuChamDiem == -1) {
            tongDiem += diem;
            soLuotCham += 1;
            float diemMoi = (float) tongDiem / (float) soLuotCham;

            new CuaHangDAO().capNhatDiemCuaHang(Integer.parseInt(paramMaCuaHang), tongDiem, soLuotCham);
            new TaiKhoanDAO().themDiemChamCuaHang(Integer.parseInt(paramMaCuaHang), Integer.parseInt(paramMaTaiKhoan), diem);
            
            PrintWriter out = response.getWriter();
        out.println(new DecimalFormat("#0.0").format(diemMoi));
        } else {
            tongDiem -= lichSuChamDiem;
            tongDiem += diem;

            float diemMoi = (float) tongDiem / (float) soLuotCham;

            new CuaHangDAO().capNhatDiemCuaHang(Integer.parseInt(paramMaCuaHang), tongDiem, soLuotCham);
            new TaiKhoanDAO().capNhatDiemChamCuaHang(Integer.parseInt(paramMaCuaHang), Integer.parseInt(paramMaTaiKhoan), diem);
            PrintWriter out = response.getWriter();
            out.println(new DecimalFormat("#0.0").format(diemMoi));
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
