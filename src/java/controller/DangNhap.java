/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.TaiKhoanDAO;
import entity.TaiKhoan;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tungthai13
 */
@WebServlet(name = "DangNhap", urlPatterns = {"/DangNhap"})
public class DangNhap extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String email = request.getParameter("email");
        String matKhau = request.getParameter("matKhau");
        String ten = request.getParameter("ten");
        String facebookId = request.getParameter("facebookId");
        String anhDaiDien = request.getParameter("anhDaiDien");
        if (facebookId != null) {
            TaiKhoan t = new TaiKhoanDAO().timTaiKhoanBangFacebookId(facebookId);
            if (t == null) {
                t = new TaiKhoan();
                t.setTen(ten);
                t.setFacebookId(facebookId);
                String url = "http://graph.facebook.com/" + facebookId + "/picture?type=square";
                t.setAnhDaiDien(url);
                boolean themTaiKhoan = new TaiKhoanDAO().themTaiKhoan(t);
                PrintWriter out = response.getWriter();
                int id = new TaiKhoanDAO().timTaiKhoanBangFacebookId(facebookId).getId();
                out.println(id);
            } else {
                PrintWriter out = response.getWriter();
                out.println(t.getId());
            }
        } else {
            TaiKhoan t = new TaiKhoanDAO().timTaiKhoanBangEmail(email);
            if (t == null) {
                t = new TaiKhoan();
                t.setEmail(email);
                t.setAnhDaiDien(anhDaiDien);
                boolean themTaiKhoan = new TaiKhoanDAO().themTaiKhoan(t);
                PrintWriter out = response.getWriter();
                int id = new TaiKhoanDAO().timTaiKhoanBangEmail(email).getId();
                out.println(id);
            } else {
                PrintWriter out = response.getWriter();
                out.println(t.getId());
            }
        }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
