/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dao.DonHangDAO;
import dao.ThucDonDAO;
import entity.MonAnChon;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 *
 * @author tungthai
 */
@WebServlet(name = "LuuDonHang", urlPatterns = {"/LuuDonHang"})
public class LuuDonHang extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

        String maCuaHangParam = request.getParameter("maCuaHang");
        String maTaiKhoanParam = request.getParameter("maTaiKhoan");
        String tongTienParam = request.getParameter("tongTien");
        String json = request.getParameter("list");
        String ngay = request.getParameter("ngay");
        String gio = request.getParameter("gio");
        String diaChi = request.getParameter("diaChi");
        String soDienThoai = request.getParameter("soDienThoai");
        String ghiChu = request.getParameter("ghiChu");
        
        int maCuaHang = Integer.parseInt(maCuaHangParam);
        int maTaiKhoan = Integer.parseInt(maTaiKhoanParam);
        int tongTien = Integer.parseInt(tongTienParam);
        
        //Xu Ly JSON bang thu vien JACKSON 2.6.3
        ObjectMapper mapper = new ObjectMapper();
        List<MonAnChon> list = mapper.readValue(json, new TypeReference<List<MonAnChon>>(){});
        for (MonAnChon monAnChon : list) {
            System.out.println(monAnChon.getTenMon() + " : " +monAnChon.getSoLuong());
        }
        ThucDonDAO tdDAO = new ThucDonDAO();
        for (MonAnChon monAnChon : list) {
            tdDAO.capNhatSoLanDatMon(monAnChon.getMaMon(), maCuaHang, monAnChon.getSoLuong());
        }
        
        boolean themDonHang = new DonHangDAO().themDonHang(maCuaHang, maTaiKhoan, list, tongTien, ngay, gio, soDienThoai, diaChi, ghiChu);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
