/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.ThucDon;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.DBConnection;

/**
 *
 * @author tungthai
 */
public class ThucDonDAO {
    Connection con;
    
    public List<ThucDon> getThucDon(int maCuaHang){
        try {
            con = DBConnection.getConnection();
            List<ThucDon> list = new ArrayList<>();
            String sql = "Select * from thucdon where maCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            ThucDon td = null;
            while(rs.next()){
                td = new ThucDon();
                td.setMaCuaHang(rs.getInt("maCuaHang"));
                td.setMaMonAn(rs.getInt("maMonAn"));
                td.setTenMonAn(rs.getString("tenMonAn"));
                td.setDonGia(rs.getInt("donGia"));
                td.setSoLanDat(rs.getInt("soLanDat"));
                td.setAnhMinhHoa(rs.getString("anhMinhHoa"));
                
                list.add(td);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ThucDonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public ThucDon getMonAn(int maMonAn, int maCuaHang){
        try {
            con = DBConnection.getConnection();
            
            String sql = "Select * from thucdon where maMonAn = ? and maCuahang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maMonAn);
            pstmt.setInt(2, maCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            ThucDon td = null;
            while(rs.next()){
                td = new ThucDon();
                td.setMaCuaHang(rs.getInt("maCuaHang"));
                td.setMaMonAn(rs.getInt("maMonAn"));
                td.setTenMonAn(rs.getString("tenMonAn"));
                td.setDonGia(rs.getInt("donGia"));
                td.setSoLanDat(rs.getInt("soLanDat"));
                td.setAnhMinhHoa(rs.getString("anhMinhHoa"));
                

            }
            
            return td;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ThucDonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public boolean capNhatSoLanDatMon(int maMonAn, int maCuaHang, int soLuong){
        try {
            con = DBConnection.getConnection();
            String sql = "Update thucdon set soLanDat = soLanDat + ? where maCuaHang = ? and maMonAn = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(2, maCuaHang);
            pstmt.setInt(3, maMonAn);
            pstmt.setInt(1, soLuong);
            
            int rs = pstmt.executeUpdate();
            return rs > 0;
        } catch (Exception e) {
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ThucDonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
