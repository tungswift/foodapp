/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.CuaHang;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import utility.DBConnection;

/**
 *
 * @author cuong
 */
public class NhaHangDAO {
    public static List<CuaHang> getAllRecords(){  
        List<CuaHang> list=new ArrayList<CuaHang>();  

        try{
            Connection con=DBConnection.getConnection();
            PreparedStatement ps=con.prepareStatement("select * from cuahang"); 
            ResultSet rs=ps.executeQuery();   
            while(rs.next()){ 
                CuaHang ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            } 
        }catch(Exception e){System.out.println(e);}  
        return list;  
    } 
//    public static int save(CuaHang c){  
//        int status=0;  
//        try{  
//            Connection con=DBConnection.getConnection();  
//            String sql = "INSERT INTO cuahang (tenCuaHang, diaChi,thoiGianMoCua,thoiGianDongCua) values (?,?,?,?)";
//            PreparedStatement statement = con.prepareStatement(sql);
//            statement.setString(1, c.getTenCuaHang());
//            statement.setString(2, c.getDiaChi()); 
////            statement.setString(3, c.getLogo()); 
//            statement.setInt(3, c.getThoiGianMoCua());
//            statement.setInt(4, c.getThoiGianDongCua()); 
//            status=statement.executeUpdate();  
//        }catch(Exception e){System.out.println(e);}  
//        return status;  
//    }
}
