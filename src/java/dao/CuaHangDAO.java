/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.CuaHang;
import entity.ThucDon;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.DBConnection;

/**
 *
 * @author tungthai
 */
public class CuaHangDAO {
    Connection con;
    
    public List<CuaHang> tatCaCuaHangPhanTrang(int trangHienTai, int phanTuTrongMotTrang){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
            String sql = "Select * from cuahang limit "+(trangHienTai-1)*phanTuTrongMotTrang+","+phanTuTrongMotTrang;
            PreparedStatement pstmt = con.prepareStatement(sql);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public List<CuaHang> timKiemCuaHang(String timKiem){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
            String search = "%"+timKiem+"%";
            String sql = "Select * from cuahang where tenCuaHang like ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, search);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public List<CuaHang> cuaHangDaMua(int maTaiKhoan){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
           
            String sql = "SELECT DISTINCT cuahang.* FROM `cuahang` JOIN donhang where cuahang.maCuaHang = donhang.maCuaHang and donhang.maTaiKhoan = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maTaiKhoan);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public List<CuaHang> timKiemCuaHangCungLoai(String timKiem, String loaiCuaHang){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
            String search = "%"+timKiem+"%";
            String sql = "Select * from cuahang where tenCuaHang like ? and loaiCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, search);
            pstmt.setString(2, loaiCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public List<CuaHang> tatCaCuaHang(){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
            String sql = "Select * from cuahang";
            PreparedStatement pstmt = con.prepareStatement(sql);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public List<CuaHang> cuaHangMoi(){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
            String sql = "Select * from cuahang ORDER BY maCuaHang DESC LIMIT 3";
            PreparedStatement pstmt = con.prepareStatement(sql);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public List<CuaHang> tatCaCuaHangCungLoai(String loaiCuaHang, int trangHienTai, int phanTuTrongMotTrang){
        List<CuaHang> list = new ArrayList<>();
        
        try {
            con = DBConnection.getConnection();
            String sql = "Select * from cuahang where loaiCuaHang = ? limit "+(trangHienTai-1)+", "+phanTuTrongMotTrang;
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, loaiCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
                list.add(ch);
            }
            
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return null;
    }
    
    public CuaHang getCuaHang(int maCuaHang){
        try {
            con = DBConnection.getConnection();

            String sql = "Select * from cuahang where maCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch = null;
            while(rs.next()){
                ch = new CuaHang();
                ch.setMaCuaHang(rs.getInt("maCuaHang"));
                ch.setTenCuaHang(rs.getString("tenCuaHang"));
                ch.setDiaChi(rs.getString("diaChi"));
                ch.setTongDiem(rs.getInt("tongDiem"));
                ch.setSoLuotCham(rs.getInt("soLuotCham"));
                ch.setThoiGianMoCua(rs.getInt("thoiGianMoCua"));
                ch.setThoiGianDongCua(rs.getInt("thoiGianDongCua"));
                ch.setLogo(rs.getString("logo"));
                ch.setLat(rs.getDouble("lat"));
                ch.setLng(rs.getDouble("lng"));
            }
            
            return ch;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public int tongSoCuaHang(){
        
        try {
            int result = 0;
            con = DBConnection.getConnection();
            String sql = "SELECT COUNT(maCuaHang) FROM cuahang";
            PreparedStatement pstmt = con.prepareStatement(sql);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                result = rs.getInt(1);
            }
            
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return -1;
    }
    
    public int tongSoCuaHangCungLoai(String loaiCuaHang){
        
        try {
            int result = 0;
            con = DBConnection.getConnection();
            String sql = "SELECT COUNT(maCuaHang) FROM cuahang where loaiCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, loaiCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            CuaHang ch;
            while(rs.next()){
                result = rs.getInt(1);
            }
            
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return -1;
    }
    
    public int getTongDiemCuaHang(int maCuaHang){
        try {
            int result = 0;
            con = DBConnection.getConnection();
            String sql = "Select tongDiem from cuahang where maCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                result = rs.getInt(1);
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return -1;
    }
    
    public int getSoLuotCham(int maCuaHang){
        try {
            int result = 0;
            con = DBConnection.getConnection();
            String sql = "Select soLuotCham from cuahang where maCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maCuaHang);
            
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                result = rs.getInt(1);
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return -1;
    }
    
    public boolean capNhatDiemCuaHang(int maCuaHang, int tongDiem, int soLuotCham){
        try {
            int result = 0;
            con = DBConnection.getConnection();
            String sql = "Update cuahang set tongDiem = ?, soLuotCham = ? where maCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, tongDiem);
            pstmt.setInt(2, soLuotCham);
            pstmt.setInt(3, maCuaHang);
            
            int rs = pstmt.executeUpdate();
            
            return rs>0;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return false;
    }
}
