/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.CuaHang;
import entity.DonHang;
import entity.MonAnChon;
import entity.MonAnDonHang;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.DBConnection;

/**
 *
 * @author tungthai
 */
public class DonHangDAO {

    Connection con;

    public boolean themDonHang(int maCuaHang, int maTaiKhoan, List<MonAnChon> list, int tongTien, String ngayGiaoHang, String gioGiaoHang, String soDienThoai, String diaChiGiaoHang, String ghiChu) {

        try {
            con = DBConnection.getConnection();
            String sql = "Insert into donhang (maTaiKhoan, tongTien, ngayDat, maCuaHang, ngayGiaoHang, gioGiaoHang, soDienThoai, diaChiGiaoHang, ghiChu) values (?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maTaiKhoan);
            pstmt.setInt(2, tongTien);
            Calendar currenttime = Calendar.getInstance();
            Date sqldate = new Date((currenttime.getTime()).getTime());
            pstmt.setDate(3, sqldate);
            pstmt.setInt(4, maCuaHang);
            pstmt.setString(5, ngayGiaoHang);
            pstmt.setString(6, gioGiaoHang);
            pstmt.setString(7, soDienThoai);
            pstmt.setString(8, diaChiGiaoHang);
            pstmt.setString(9, ghiChu);
            int rs = pstmt.executeUpdate();
            
            for (MonAnChon monAnChon : list) {
                sql = "Insert into monan_donhang (maDonHang, maMonAn, soLuong) values (LAST_INSERT_ID(),?,?)";
                pstmt = con.prepareStatement(sql);
                pstmt.setInt(1, monAnChon.getMaMon());
                pstmt.setInt(2, monAnChon.getSoLuong());

                pstmt.executeUpdate();
            }

            return rs > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DonHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }
    
    public List<DonHang> danhSachDonHang(int maTaiKhoan) {

        try {
            con = DBConnection.getConnection();
            String sql = "Select * from donhang where maTaiKhoan = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maTaiKhoan);

            ResultSet rs = pstmt.executeQuery();
            List<DonHang> list = new ArrayList<>();
            DonHang dh;
            while(rs.next()){
                dh = new DonHang();
                dh.setMaDonHang(rs.getInt("maDonHang"));
                dh.setTongTien(rs.getInt("tongTien"));
                dh.setNgayDat(rs.getDate("ngayDat"));
                dh.setTrangThaiDonHang(rs.getString("trangThaiDonHang"));
                dh.setMaTaiKhoan(rs.getInt("maTaiKhoan"));
                dh.setMaCuaHang(rs.getInt("maCuaHang"));
                dh.setNgayGiaoHang(rs.getString("ngayGiaoHang"));
                dh.setGioGiaoHang(rs.getString("gioGiaoHang"));
                dh.setDiaChiGiaoHang(rs.getString("diaChiGiaoHang"));
                dh.setSoDienThoai(rs.getString("soDienThoai"));
                dh.setGhiChu(rs.getString("ghiChu"));
                list.add(dh);
            }
            

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DonHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }
    
    public List<MonAnDonHang> chiTietDonHang(int maDonHang) {

        try {
            con = DBConnection.getConnection();
            String sql = "Select * from monan_donhang where maDonHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maDonHang);

            ResultSet rs = pstmt.executeQuery();
            List<MonAnDonHang> list = new ArrayList<>();
            MonAnDonHang m;
            while(rs.next()){
                m = new MonAnDonHang();
                m.setMaMonAn(rs.getInt("maMonAn"));
                m.setSoLuong(rs.getInt("soLuong"));
                m.setMaDonHang(maDonHang);
                list.add(m);
            }
            

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(DonHangDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }
}
