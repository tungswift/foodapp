/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.CuaHang;
import entity.TaiKhoan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utility.DBConnection;

/**
 *
 * @author tungthai13
 */
public class TaiKhoanDAO {

    Connection con;

    public boolean themTaiKhoan(TaiKhoan t) {
        try {
            con = DBConnection.getConnection();
            String sql = "Insert into taikhoan (ten, email, matKhau, anhDaiDien, facebookId) values (?, ?, ?, ?, ?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, t.getTen());
            pstmt.setString(2, t.getEmail());
            pstmt.setString(3, t.getMatKhau());
            pstmt.setString(4, t.getAnhDaiDien());
            pstmt.setString(5, t.getFacebookId());

            int result = pstmt.executeUpdate();

            return result > 0;

        } catch (Exception e) {
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }

    public TaiKhoan timTaiKhoanBangEmail(String email) {

        try {
            con = DBConnection.getConnection();
            String sql = "Select * from taikhoan where email = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, email);
            ResultSet rs = pstmt.executeQuery();
            TaiKhoan t = null;
            while (rs.next()) {
                t = new TaiKhoan();
                t.setId(rs.getInt("id"));
                t.setTen(rs.getString("ten"));
                t.setMatKhau(rs.getString("matKhau"));
                t.setEmail(rs.getString("email"));
                t.setAnhDaiDien(rs.getString("anhDaiDien"));
                t.setFacebookId(rs.getString("facebookId"));
            }

            return t;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }

    public TaiKhoan timTaiKhoanBangFacebookId(String facebookId) {

        try {
            con = DBConnection.getConnection();
            String sql = "Select * from taikhoan where facebookId = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, facebookId);

            ResultSet rs = pstmt.executeQuery();
            TaiKhoan t = null;
            while (rs.next()) {
                t = new TaiKhoan();
                t.setId(rs.getInt("id"));
                t.setTen(rs.getString("ten"));
                t.setMatKhau(rs.getString("matKhau"));
                t.setEmail(rs.getString("email"));
                t.setAnhDaiDien(rs.getString("anhDaiDien"));
                t.setFacebookId(rs.getString("facebookId"));
            }

            return t;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }

    public TaiKhoan timTaiKhoanBangEmailVaMatKhau(String email, String matKhau) {

        try {
            con = DBConnection.getConnection();
            String sql = "Select * from taikhoan where email = ? and matKhau = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, email);
            pstmt.setString(2, matKhau);
            ResultSet rs = pstmt.executeQuery();
            TaiKhoan t = null;
            while (rs.next()) {
                t = new TaiKhoan();
                t.setId(rs.getInt("id"));
                t.setTen(rs.getString("ten"));
                t.setMatKhau(matKhau);
                t.setEmail(email);
                t.setAnhDaiDien(rs.getString("anhDaiDien"));
                t.setFacebookId(rs.getString("facebookId"));
            }

            return t;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }

    public int diemChamCuaHang(int maCuaHang, int maTaiKhoan) {
        try {
            con = DBConnection.getConnection();
            String sql = "Select * from danhgiacuahang where maCuaHang = ? and maTaiKhoan = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maCuaHang);
            pstmt.setInt(2, maTaiKhoan);
            ResultSet rs = pstmt.executeQuery();
            int diemCham = -1;
            while (rs.next()) {
                diemCham = rs.getInt("diemCham");
            }

            return diemCham;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return -1;
    }

    public boolean capNhatDiemChamCuaHang(int maCuaHang, int maTaiKhoan, int diemCham) {
        try {
            con = DBConnection.getConnection();
            String sql = "Update danhgiacuahang set diemCham = ? where maCuaHang = ? and maTaiKhoan = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, diemCham);
            pstmt.setInt(2, maCuaHang);
            pstmt.setInt(3, maTaiKhoan);
            int rs = pstmt.executeUpdate();

            return rs > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }

    public boolean themDiemChamCuaHang(int maCuaHang, int maTaiKhoan, int diemCham) {
        try {
            con = DBConnection.getConnection();
            String sql = "Insert into danhgiacuahang (maCuaHang, maTaiKhoan, diemCham) values (?,?,?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, maCuaHang);
            pstmt.setInt(2, maTaiKhoan);
            pstmt.setInt(3, diemCham);
            int rs = pstmt.executeUpdate();

            return rs > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return false;
    }

    public List<TaiKhoan> danhSachTaiKhoan() {
        try {
            con = DBConnection.getConnection();
            List<TaiKhoan> list = new ArrayList<>();
            String sql = "Select * from taikhoan LIMIT 10";
            PreparedStatement pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            TaiKhoan t = null;
            while (rs.next()) {
                t = new TaiKhoan();
                t.setId(rs.getInt("id"));
                t.setTen(rs.getString("ten"));
                t.setMatKhau(rs.getString("matKhau"));
                t.setEmail(rs.getString("email"));
                t.setAnhDaiDien(rs.getString("anhDaiDien"));
                t.setFacebookId(rs.getString("facebookId"));

                list.add(t);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CuaHangDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }
}
