/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.LoginBean;
import java.sql.*;
import utility.DBConnection;

/**
 *
 * @author cuong
 */
public class LoginDAO {
    public static String loginCheck(LoginBean loginBean){
        String query="select * from user_admin where email=? and password=?";
        
        try{
            Connection con=DBConnection.getConnection();
            PreparedStatement ps=con.prepareStatement(query);
            ps.setString(1,loginBean.getEmail());
            ps.setString(2,loginBean.getPassword());
            
            ResultSet rs=ps.executeQuery();
            
            if(rs.next()){
                return "true";
            }
            else{
                return "false";
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return "error";
    } 
}
