/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author tungthai13
 */
public class DBConnection {
    public static String ip, port, sql, user, pass;
    public static Connection getConnection() throws SQLException {
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            // XAMPP Mysql
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/foodapp?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8","root", "");
            //conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/foodapp?autoReconnect=true&useSSL=false","root", "");
            //conn = DriverManager.getConnection("jdbc:mysql://"+ip+":"+port+"/book?autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8",user, pass);
            
            // Mysql server workbench
            //conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/book?autoReconnect=true&useSSL=false","root", "root");
            
            if(conn!= null){
                System.out.println();
            }
            return conn;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static void main(String[] args) throws SQLException {
        System.out.println(getConnection());
    }
    
}
