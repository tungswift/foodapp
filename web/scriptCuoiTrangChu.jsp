<%-- 
    Document   : scriptTrangChu
    Created on : Dec 1, 2017, 6:42:44 PM
    Author     : tungthai13
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--Script slide-->
<script type="text/javascript" src="js/slideShow.js"></script>

<!--Script tab-->
<script type="text/javascript" src="js/tab.js"></script>

<!--Lấy vị trí hiện tại-->
<script type="text/javascript" src="js/getLocation.js"></script>

<script>
    var listMaTaiKhoan = document.getElementsByClassName("maTaiKhoan");
    for (var i = 0; i < listMaTaiKhoan.length; i++) {
        listMaTaiKhoan[i].value = parseInt(localStorage.getItem("maTaiKhoan"));
    }
</script>
