<%-- 
    Document   : danhsachdonhang
    Created on : Dec 22, 2017, 2:28:39 PM
    Author     : tungthai
--%>

<%@page import="dao.DonHangDAO"%>
<%@page import="entity.DonHang"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.min.js"></script>
        
        <style>
            body{
                margin: 0 10px 0 10px;
            }
        </style>
    </head>
    <body>
        <table class="table table-striped">
            <tr>
                <th>STT</th>
                <th>Mã đơn hàng</th>
                <th>Ngày đặt hàng</th>
                <th>Tổng tiền</th>
                <th>Trạng thái đơn hàng</th>
                <th>Ngày giao hàng</th>
                <th>Giờ giao hàng</th>
                <th>Địa chỉ giao hàng</th>
                <th>Ghi chú</th>
                <th>Chi tiết</th>
            </tr>
            <% 
                int i = 1; 
                int maTaiKhoan = Integer.parseInt(String.valueOf(request.getAttribute("maTaiKhoan")));
                List<DonHang> list = new DonHangDAO().danhSachDonHang(maTaiKhoan);
                for (DonHang item : list) {
                    
            %>
            <tr>
                <td><%=i%> </td>
                <td><%=item.getMaDonHang()%></td>
                <td><%=item.getNgayDat()%></td>
                <td><%=item.getTongTien()%></td>
                <td><%=item.getTrangThaiDonHang()%></td>
                <td><%=item.getNgayGiaoHang()%></td>
                <td><%=item.getGioGiaoHang()%></td>
                <td><%=item.getDiaChiGiaoHang()%></td>
                <td><%=item.getGhiChu()%></td>
                <td><a href="chitietdonhang.jsp?maDonHang=<%=item.getMaDonHang()%>&maTaiKhoan=<%=item.getMaTaiKhoan()%>&maCuaHang=<%=item.getMaCuaHang()%>">Xem</a></td>
            </tr>
            <% i++; } %>
        </table>
    </body>
</html>
