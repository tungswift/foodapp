<%-- 
    Document   : head
    Created on : Nov 6, 2017, 7:52:24 PM
    Author     : tungthai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="google-signin-client_id" content="303075933611-8npb4oo9pcfbdrgo11havdt9ghi239l9.apps.googleusercontent.com">

<title>Food</title>
<!-- <link rel="stylesheet" href="css/w3.css">  -->
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/app.css?v=2">
<link rel="stylesheet" href="css/phantrang.css">
<link rel="stylesheet" href="css/jquery.rateyo.min.css">
<style>
    .address {
        height: 56px;
    }
</style>

<!--Script chung phần chọn cửa hàng-->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="js/dautrangchu.js?v=1"></script>
