<%-- 
    Document   : chitietdonhang
    Created on : Dec 22, 2017, 3:31:17 PM
    Author     : tungthai
--%>

<%@page import="entity.CuaHang"%>
<%@page import="dao.CuaHangDAO"%>
<%@page import="entity.ThucDon"%>
<%@page import="entity.MonAnChon"%>
<%@page import="dao.ThucDonDAO"%>
<%@page import="entity.MonAnDonHang"%>
<%@page import="dao.DonHangDAO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.min.js"></script>
        <style>
            body{
                margin: 0 10px 0 10px;
            }
        </style>
    </head>
    <body>
        <h1>Chi tiết đơn hàng</h1>
        <% 
            int maCuaHang = Integer.parseInt(String.valueOf(request.getParameter("maCuaHang"))); 
            CuaHang ch = new CuaHangDAO().getCuaHang(maCuaHang);
        %>
        <h4>Cửa hàng: <%=ch.getTenCuaHang()%></h2>
        <h4>Địa chỉ: <%=ch.getDiaChi()%></h2>
        <table class="table table-striped">
            <tr>
                <th>STT</th>
                <th>Tên món ăn</th>
                <th>Số lượng</th>
                <th>Đơn giá</th>
                <th>Tổng</th>
            </tr>
            <% 
                int i = 1; 
                int maDonHang = Integer.parseInt(String.valueOf(request.getParameter("maDonHang")));

                List<MonAnDonHang> list = new DonHangDAO().chiTietDonHang(maDonHang);
                for (MonAnDonHang item : list) {
                    ThucDon t = new ThucDonDAO().getMonAn(item.getMaMonAn(), maCuaHang);
                    
            %>
            <tr>
                <td><%=i%> </td>
                <td><%=t.getTenMonAn()%></td>
                <td><%=item.getSoLuong()%></td>
                <td><%=t.getDonGia()%></td>
                <td><%=item.getSoLuong()*t.getDonGia()%></td>
            </tr>
            <% i++; } %>
        </table>
    </body>
</html>
