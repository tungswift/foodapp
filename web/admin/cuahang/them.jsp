<%-- 
    Document   : them
    Created on : Dec 21, 2017, 9:40:40 PM
    Author     : cuong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% request.setCharacterEncoding("UTF-8"); %>
<%@page import="controller.*"%>
<html>
    <head>
        <%@include file="../header.jsp" %>
    </head>
    <body class="w3-light-grey">
        
        <%@include file="../nav.jsp" %>

        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <div class="w3-main" style="margin-left:300px;margin-top:43px;"> 
            <header class="w3-container" style="padding-top:22px">
                <h5><b><i class="fa fa-dashboard"></i> Công Việc</b></h5>
            </header> 
            <div class="content w3-container">
                <div class="container"> 
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="">Cửa hàng</h3>
                        </div>
                        <div class="col-md-12 w3-margin-top">
                            <form method="POST" action="uploadImage.jsp" enctype="multipart/form-data" >
                                <div class="form-group">
                                    <label for="colFormLabel" class="col-form-label">Tên cửa hàng</label>
                                    <div>
                                        <input name="tenCuaHang" type="text" class="form-control" id="colFormLabel" placeholder="Nhập tên cửa hàng">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="colFormLabel" class="col-form-label">Địa chỉ</label>
                                    <div>
                                        <input name="diaChi" type="text" class="form-control" id="colFormLabel" placeholder="Nhập địa chỉ cửa hàng">
                                    </div>
                                </div> 
                                <div class="row form-group">
                                    <div class="col">
                                        <label for="timeopen">Thời gian mở cửa</label>
                                        <input name="thoiGianMoCua" type="number" min="0" max="24" id="timeopen" class="form-control" placeholder="Thời gian mở cửa">
                                    </div>
                                    <div class="col">
                                        <label for="timeclose">Thời gian đóng cửa</label>
                                        <input name="thoiGianDongCua" type="number" min="0" max="24" id="timeclose" class="form-control" placeholder="Thời gian đóng cửa">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-3"> 
                                        <input class="btn btn-primary" type="submit" value="submit">Thêm mới</input>
                                    </div>  
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <%@include file="../footer.jsp" %>                 
    </body>
</html>x
