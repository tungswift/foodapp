<%-- 
    Document   : sua
    Created on : Dec 21, 2017, 9:39:59 PM
    Author     : cuong
--%>

<%@page import="java.util.logging.Level"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../header.jsp" %>
    </head>
    <body class="w3-light-grey">
        
        <%@include file="../nav.jsp" %>
        <%@page import="dao.CuaHangDAO,entity.CuaHang"%>
        <%@page import="java.sql.*,java.util.*,utility.DBConnection"%> 
        <% request.setCharacterEncoding("UTF-8"); %>
  
        <%  
            Connection con;
            String maCuaHang=request.getParameter("id");
            try {
            con = DBConnection.getConnection();

            String sql = "Select * from cuahang where maCuaHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, maCuaHang);
            
            ResultSet rs = pstmt.executeQuery(); 
            if(rs != null) {
                rs.next();

                %>
               
        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <div class="w3-main" style="margin-left:300px;margin-top:43px;"> 
            <header class="w3-container" style="padding-top:22px">
                <h5><b><i class="fa fa-dashboard"></i> Công Việc</b></h5>
            </header> 
            <div class="content w3-container">
                <div class="container"> 
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="">Sửa cửa hàng</h3>
                        </div>
                        <div class="col-md-12 w3-margin-top">
                            <form method="POST" action="suaCuaHang.jsp" enctype="" >
                                <div class="form-group">
                                    <label for="colFormLabel" class="col-form-label">Tên cửa hàng</label>
                                    <div>
                                        <input name="maCuaHang" value="<%=maCuaHang%>" type="hidden" >
                                        <input name="tenCuaHang" value="<%=rs.getString("tenCuaHang")%>" type="text" class="form-control" id="colFormLabel" placeholder="Nhập tên cửa hàng">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="colFormLabel" class="col-form-label">Địa chỉ</label>
                                    <div>
                                        <input name="diaChi" type="text" value="<%=rs.getString("diaChi")%>" class="form-control" id="colFormLabel" placeholder="Nhập địa chỉ cửa hàng">
                                    </div>
                                </div> 
                                <div class="row form-group">
                                    <div class="col">
                                        <label for="timeopen">Thời gian mở cửa</label>
                                        <input name="thoiGianMoCua" type="text" value="<%=rs.getString("thoiGianMoCua")%>" id="timeopen" class="form-control" placeholder="Thời gian mở cửa">
                                    </div>
                                    <div class="col">
                                        <label for="timeclose">Thời gian đóng cửa</label>
                                        <input name="thoiGianDongCua" type="text" value="<%=rs.getString("thoiGianDongCua")%>" id="timeclose" class="form-control" placeholder="Thời gian đóng cửa">
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-3"> 
                                        <button class="btn btn-primary" type="submit" value="submit">Sửa</button> 
                                    </div>  
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <%
            } 

            } catch(Exception e) {
                System.out.print(e);
                e.printStackTrace();
            }
        %> 
        <%@include file="../footer.jsp" %>                 
    </body>
</html>
