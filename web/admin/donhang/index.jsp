<%-- 
    Document   : index
    Created on : Dec 19, 2017, 3:43:38 PM
    Author     : cuong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>  
<%@page import="dao.NhaHangDAO"%>
<%@page import="entity.*,java.util.*,utility.DBConnection"%>
<%@page import="java.sql.*"%>

<html>
    <head>
        <%@include file="../header.jsp" %>
    </head>
    <style>
        table{
            text-align: center;
        }
    </style>
    <body class="w3-light-grey">
        
        <%@include file="../nav.jsp" %>

        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <div class="w3-main" style="margin-left:300px;margin-top:43px;"> 
            <header class="w3-container" style="padding-top:22px">
                <h5><b><i class="fa fa-dashboard"></i> Công Việc</b></h5>
            </header> 
            <div class="content w3-container">
                <div class="container">
                    <div class="row"> 

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Danh sách món ăn</h1>
                        </div>
                    </div>  
                    <div class="row">
                        <table class="table table-light">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>  
                                    <th scope="col">Ngày Đặt</th>
                                    <th scope="col">Tổng Tiền</th>
                                    <th scope="col">Trạng Thái Đơn Hàng</th> 
                                    <th scope="col">Ngày Giao Hàng</th>
                                    <th scope="col">Giờ Giao Hàng</th>
                                    <th scope="col">Địa Chỉ</th>
                                    <th scope="col">Số Điện Thoại</th>
                                    <th scope="col">Sự kiện</th>
                                </tr>
                            </thead>
                            <tbody> 
                            <%
                                try{
                                Connection con;
                                con = DBConnection.getConnection(); 
                                String sql = "Select * from donhang";
                                PreparedStatement pstmt = con.prepareStatement(sql);
                                ResultSet rs = pstmt.executeQuery();
                                int count =0;
                                while(rs.next()){
                                    count++;
                            %>
                                <tr>
                                    <td><%=count%></td> 
                                    <td><%=rs.getDate("ngayDat")%></td>
                                    <td><%=rs.getInt("tongTien")%></td>  
                                    <td><%=rs.getString("trangThaiDonHang")%></td> 
                                    <td><%=rs.getString("ngayGiaoHang")%></td> 
                                     <td><%=rs.getString("gioGiaoHang")%></td> 
                                      <td><%=rs.getString("diaChiGiaoHang")%></td> 
                                    <td><%=rs.getString("soDienThoai")%></td>
                                    <td><a href="../donhang/sua.jsp?id=<%=rs.getInt("maDonHang")%>" class="btn-success w3-padding"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                </tr>
                            <%
                            }
                                con.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="../footer.jsp" %>                 
    </body>
</html>
