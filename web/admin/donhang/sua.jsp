<%-- 
    Document   : sua
    Created on : Dec 21, 2017, 9:39:59 PM
    Author     : cuong
--%>
<%@ page import="java.sql.*,java.util.Date" %> 
<%@page import="java.util.logging.Level"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../header.jsp" %>
    </head>
    <body class="w3-light-grey">
        
        <%@include file="../nav.jsp" %>
        <%@page import="dao.CuaHangDAO,entity.CuaHang"%>
        <%@page import="java.sql.*,java.util.*,utility.DBConnection"%> 
        <% request.setCharacterEncoding("UTF-8"); %>
  
        <%  
            Connection con;
            String maDonHang=request.getParameter("id");
            try {
            con = DBConnection.getConnection();

            String sql = "Select * from donhang where maDonHang = ?";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, maDonHang);
            
            ResultSet rs = pstmt.executeQuery(); 
            if(rs != null) {
                rs.next();

                %>
               
        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <div class="w3-main" style="margin-left:300px;margin-top:43px;"> 
            <header class="w3-container" style="padding-top:22px">
                <h5><b><i class="fa fa-dashboard"></i> Công Việc</b></h5>
            </header> 
            <div class="content w3-container">
                <div class="container"> 
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="">Lịch sử đơn hàng</h3>
                        </div>
                        <div class="col-md-12 w3-margin-top">
                            <form method="POST" action="suaDonHang.jsp" enctype="" > 
                                <div class="row form-group">
                                    <div class="col">
                                        <label for="ngayGiaoHang">Ngày giao hàng</label>
                                        <input name="ngayGiaoHang" type="text" value="<%=rs.getString("ngayGiaoHang")%>" id="timeopen" class="form-control" >
                                        <input name="maDonHang" type="hidden" value="<%=maDonHang%>" id="timeopen" class="form-control" >
                                    </div>
                                    <div class="col">
                                        <label for="timeclose">Giờ giao hàng</label>
                                        <input name="gioGiaoHang" type="text" value="<%=rs.getString("gioGiaoHang")%>" id="timeclose" class="form-control" >
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col">
                                        <label for="timeopen">Địa  chỉ giao hàng</label>
                                        <input name="diaChiGiaoHang" type="text" value="<%=rs.getString("diaChiGiaoHang")%>" id="timeopen" class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="timeclose">Số điện thoai</label>
                                        <input name="soDienThoai" type="text" value="<%=rs.getString("soDienThoai")%>" id="timeclose" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelect1">Trạng thái đơn hàng</label>
                                    <select class="form-control" id="exampleSelect1" name="trangThaiDonHang">
                                        <option>Chờ xử lí</option>
                                        <option>Đã xử lý</option>
                                        <option>Đang giao hàng</option>
                                        <option>Đã giao hàng</option> 
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3"> 
                                        <button class="btn btn-primary" type="submit" value="submit">Sửa</button> 
                                    </div>  
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <%
            } 

            } catch(Exception e) {
                System.out.print(e);
                e.printStackTrace();
            }
        %> 
        <%@include file="../footer.jsp" %>                 
    </body>
</html>
