<%-- 
    Document   : loginRequestHandler
    Created on : Dec 19, 2017, 1:51:40 PM
    Author     : cuong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="dao.LoginDAO"%>
    <jsp:useBean id="loginBean" class="entity.LoginBean" scope="session"/>
    <jsp:setProperty name="loginBean" property="*"/>

<%
    String result=LoginDAO.loginCheck(loginBean);

    if(result.equals("true")){
        session.setAttribute("email",loginBean.getEmail());
        response.sendRedirect("../login");
    }

    if(result.equals("false")){
        response.sendRedirect("index.jsp?status=false");
    }

    if(result.equals("error")){
        response.sendRedirect("index.jsp?status=error");
    }

%>