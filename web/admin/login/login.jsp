<%-- 
    Document   : login
    Created on : Nov 8, 2017, 9:06:40 AM
    Author     : tungthai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../header.jsp" %>
    </head>
    <body class="w3-light-grey">
        <%
        String email=(String)session.getAttribute("email");
        
        //redirect user to home page if already logged in
        if(email!=null){
            response.sendRedirect("login");
        }
 
        String status=request.getParameter("status");
        
        if(status!=null){
            if(status.equals("false")){
                   out.print("Incorrect login details!");                       
            }
            else{
                out.print("Some error occurred!");
            }
        }
        %>
        <div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
            <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
            <span class="w3-bar-item w3-left"><img src="../../image/logo.png" alt=""></span>
            <span class="w3-bar-item w3-right">
                <div class="w3-container">  
                    <button onclick="document.getElementById('id01').style.display = 'block'" class="w3-button w3-green w3-large">Đăng Nhập</button>

                    <div id="id01" class="w3-modal">
                        <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

                            <div class="w3-center"><br>
                                <span onclick="document.getElementById('id01').style.display = 'none'" class="w3-button w3-xlarge w3-transparent w3-display-topright" title="Close Modal">×</span>
                                <img src="../../image/avatar-admin.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
                            </div>

                            <form class="w3-container" action="loginRequestHandler.jsp">
                                <div class="w3-section">
                                    <label><b>Email</b></label>
                                    <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Nhập địa chỉ email" name="email" required>
                                    <label><b>Mật Khẩu</b></label>
                                    <input class="w3-input w3-border" type="password" placeholder="Nhập Mật Khẩu" name="password" required>
                                    <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Đăng Nhập</button>
                                    <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Remember me
                                </div>
                            </form>

                            <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                                <button onclick="document.getElementById('id01').style.display = 'none'" type="button" class="w3-button w3-red">Hủy</button>
                                 
                            </div>

                        </div>
                    </div>
                </div>
            </span>
        </div>

        <header> 
            <!-- Third Grid: Swing By & Contact -->
            <div class="w3-row w3-margin-top" id="contact">
                <div class="w3-half w3-dark-grey w3-container w3-center" style="height:700px">
                    <div class="w3-padding-64">
                        <h1>Swing By</h1>
                    </div>
                    <div class="w3-padding-64">
                        <p>Đặt đồ ăn nhanh</p>
                        <p>Hà Nội, Việt Nam</p>
                        <p>0167 2624 165</p>
                        <p>dangvancuong196@gmail.com</p>
                    </div>
                </div>
                <div class="w3-half w3-teal w3-container" style="height:700px">
                    <div class="w3-padding-64 w3-padding-large">
                        <h1>Contact</h1>
                        <p class="w3-opacity">GET IN TOUCH</p>
                        <form class="w3-container w3-card w3-padding-32 w3-white" action="/action_page.php" target="_blank">
                            <div class="w3-section">
                                <label>Name</label>
                                <input class="w3-input" style="width:100%;" type="text" required name="Name">
                            </div>
                            <div class="w3-section">
                                <label>Email</label>
                                <input class="w3-input" style="width:100%;" type="text" required name="Email">
                            </div>
                            <div class="w3-section">
                                <label>Message</label>
                                <input class="w3-input" style="width:100%;" type="text" required name="Message">
                            </div>
                            <button type="submit" class="w3-button w3-teal w3-right">Send</button>
                        </form>
                    </div>
                </div>
            </div> 
        </header>
    </body>
    
    <%@include file="../footer.jsp" %>
</html>
