<%-- 
    Document   : logout
    Created on : Dec 19, 2017, 2:05:29 PM
    Author     : cuong
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 
<%
    session.invalidate();
    response.sendRedirect("login.jsp");
%>
