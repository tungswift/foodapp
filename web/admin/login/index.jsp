<%-- 
    Document   : index
    Created on : Nov 8, 2017, 9:02:32 AM
    Author     : tungthai
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>  
<%@page import="dao.NhaHangDAO"%>
<%@page import="entity.*,java.util.* "%>

<html>
    <head>
        <%@include file="../header.jsp" %>
    </head>
    <body class="w3-light-grey">
        
        <%@include file="../nav.jsp" %>

        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <div class="w3-main" style="margin-left:300px;margin-top:43px;"> 
            <header class="w3-container" style="padding-top:22px">
                <h5><b><i class="fa fa-dashboard"></i> Công Việc</b></h5>
            </header> 
            <div class="content w3-container">
                <div class="container">
                    <div class="row">
                        <div class=" col-md-12">
                            <a href="../cuahang/them.jsp" class="btn w3-blue w3-right w3-margin"> <i class="fa fa-plus" aria-hidden="true"></i> Thêm Cửa Hàng</a>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Danh sách cửa hàng</h1>
                        </div>
                    </div> 
                    <%   
                        List<CuaHang> list = NhaHangDAO.getAllRecords();
                        request.setAttribute("list", list);
                        int count = 0;
                        
                    %>  
                    <div class="row">
                        <table class="table table-light">
                            <thead>
                                <tr>
                                    <th scope="col">#</th> 
                                    <th scope="col">Tên</th>
                                    <th scope="col">Địa chỉ</th>
                                    <th scope="col">Điểm</th>
                                    <th scope="col">Số Lượt chấm</th> 
                                    <th scope="col">logo</th> 
                                    <th scope="col" colspan="2">Sự Kiện</th>
                                </tr>
                            </thead>
                            <tbody> 
                            <% for (CuaHang ch : list) {
                                count++;
                                %>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=ch.getTenCuaHang()%></td>
                                    <td><%=ch.getDiaChi()%></td>
                                    <td><%=ch.getTongDiem()%></td>
                                    <td><%=ch.getSoLuotCham()%></td> 
                                    <td><img src="../../image/<%=ch.getLogo()%>" alt="" width="70"></td> 
                                    <td><a href="../cuahang/sua.jsp?id=<%=ch.getMaCuaHang()%>" class="btn-success w3-padding"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                    <td><a href="../cuahang/xoa.jsp?id=<%=ch.getMaCuaHang()%>" class="btn-success w3-padding w3-red"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                    
                                </tr>
                                <% }%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="../footer.jsp" %>                 
    </body>
</html>
