-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 03, 2018 at 11:47 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `foodapp`
--
CREATE DATABASE IF NOT EXISTS `foodapp` DEFAULT CHARACTER SET utf8 COLLATE utf8_vietnamese_ci;
USE `foodapp`;

-- --------------------------------------------------------

--
-- Table structure for table `cuahang`
--

DROP TABLE IF EXISTS `cuahang`;
CREATE TABLE IF NOT EXISTS `cuahang` (
  `maCuaHang` int(11) NOT NULL AUTO_INCREMENT,
  `tenCuaHang` varchar(200) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `diaChi` varchar(200) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `logo` varchar(300) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `loaiCuaHang` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `thoiGianMoCua` int(11) DEFAULT NULL,
  `thoiGianDongCua` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `tongDiem` int(11) NOT NULL DEFAULT '0',
  `soLuotCham` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`maCuaHang`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `cuahang`
--

INSERT INTO `cuahang` (`maCuaHang`, `tenCuaHang`, `diaChi`, `logo`, `loaiCuaHang`, `thoiGianMoCua`, `thoiGianDongCua`, `lat`, `lng`, `tongDiem`, `soLuotCham`) VALUES
(1, 'Chả cá Anh Vũ', '116-K1 Giảng Võ, Hà Nội', 'chacaanhvu.jpeg', 'Đồ ăn', 10, 22, 21.027081, 105.824167, 17, 2),
(2, 'Bánh mì Minh Nhật', '22 Trần Duy Hưng, Quận Cầu Giấy, Hà Nội', 'banhmiminhnhat.jpg', 'Đồ ăn', 7, 22, 21.013781, 105.802689, 12, 2),
(3, 'Ding Tea - Cầu Giấy', '180 Cầu Giấy, Quận Cầu Giấy, Hà Nội', 'dingtea.png', 'Đồ uống', 8, 22, 21.03368, 105.797861, 17, 2),
(4, 'Le Castella Viet Nam', '7 Huỳnh Thúc Kháng, Quận Đống Đa, Hà Nội', 'LeCastella.jpg', 'Bánh', 9, 22, 21.017049, 105.813588, 8, 1),
(5, 'Bánh Trôi Tàu Phạm Bằng', '30 Hàng Giầy, Quận Hoàn Kiếm, Hà Nội', 'BanhTroiTauPhamBang.jpg', 'Bánh', 15, 21, 21.035636, 105.851123, 7, 1),
(6, 'Cơm Tấm Hằng', '12 Ngõ 16 Huỳnh Thúc Kháng, Quận Đống Đa, Hà Nội', 'ComTamHang.jpg', 'Đồ Ăn', 9, 20, 21.019941, 105.809576, 13, 2),
(7, 'Trà Sữa Gong Cha - Giảng Võ', 'Tầng 1, D2 Giảng Võ, Quận Ba Đình, Hà Nội', 'gongcha.jpg', 'Đồ Uống', 8, 22, 21.025246, 105.82136, 8, 1),
(8, 'Trà Sữa Heekcaa - Lê Đại Hành', '7C Lê Đại Hành, Quận Hai Bà Trưng, Hà Nội', 'heekcaa.jpg', 'Đồ Uống', 8, 22, 21.009494, 105.850388, 9, 1),
(9, 'McDonald\'s - Hàng Bài', '2 Hàng Bài, Quận Hoàn Kiếm, Hà Nội', 'McDonal.jpg', 'Đồ Ăn', 9, 21, 21.025504, 105.85319, 19, 2),
(10, 'Let\'eat - Healthy Food Online', 'Ngõ 470 Nguyễn Trãi, Quận Thanh Xuân, Hà Nội', 'LetsEat.jpg', 'Đồ Ăn', 15, 20, 20.995368, 105.808969, 6, 1),
(11, 'Khoai Bao - Khoai Lang Mật Đà Lạt', '155 Giáp Nhất, Quận Thanh Xuân, Hà Nội', 'KhoaiBao.jpg', 'Đồ Ăn', 8, 21, 21.00543, 105.816267, 45, 6),
(12, 'Master Tea', '24B Lò Đúc, P. Phạm Đình Hổ, Quận Hai Bà Trưng, Hà Nội', 'MasterTea.jpg', 'Đồ Uống', 9, 22, 21.017825, 105.855485, 56, 6),
(13, 'Ô2Ô - Bánh Cuốn Phủ Lý', '50 Nguyễn Du, Quận Hai Bà Trưng, Hà Nội', 'O2O.jpg', 'Đồ Ăn', 7, 22, 21.019302, 105.84816, 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `danhgiacuahang`
--

DROP TABLE IF EXISTS `danhgiacuahang`;
CREATE TABLE IF NOT EXISTS `danhgiacuahang` (
  `maCuaHang` int(11) NOT NULL,
  `maTaiKhoan` int(11) NOT NULL,
  `diemCham` int(11) DEFAULT NULL,
  PRIMARY KEY (`maCuaHang`,`maTaiKhoan`),
  KEY `FK_MaTaiKhoanDanhGia` (`maTaiKhoan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `danhgiacuahang`
--

INSERT INTO `danhgiacuahang` (`maCuaHang`, `maTaiKhoan`, `diemCham`) VALUES
(1, 11, 8),
(1, 12, 9),
(2, 11, 4),
(2, 12, 8),
(3, 11, 10),
(3, 12, 7);

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

DROP TABLE IF EXISTS `donhang`;
CREATE TABLE IF NOT EXISTS `donhang` (
  `maDonHang` int(11) NOT NULL AUTO_INCREMENT,
  `maTaiKhoan` int(11) NOT NULL,
  `ngayDat` date NOT NULL,
  `tongTien` int(11) NOT NULL,
  `trangThaiDonHang` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL DEFAULT 'Chờ xử lý',
  `maCuaHang` int(11) NOT NULL,
  `ngayGiaoHang` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `gioGiaoHang` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `soDienThoai` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `diaChiGiaoHang` varchar(500) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `ghiChu` varchar(500) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`maDonHang`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`maDonHang`, `maTaiKhoan`, `ngayDat`, `tongTien`, `trangThaiDonHang`, `maCuaHang`, `ngayGiaoHang`, `gioGiaoHang`, `soDienThoai`, `diaChiGiaoHang`, `ghiChu`) VALUES
(1, 12, '2017-12-22', 220000, 'Chờ xử lý', 1, '', NULL, NULL, '', ''),
(2, 12, '2017-12-22', 810000, 'Chờ xử lý', 1, '', NULL, NULL, '', ''),
(3, 12, '2017-12-22', 160000, 'Chờ xử lý', 3, '', NULL, NULL, '', ''),
(4, 12, '2017-12-22', 416000, 'Chờ xử lý', 3, '', NULL, NULL, '', ''),
(5, 11, '2017-12-24', 150000, 'Chờ xử lý', 1, '', NULL, NULL, '', ''),
(6, 11, '2017-12-24', 60000, 'Chờ xử lý', 1, NULL, NULL, NULL, NULL, NULL),
(7, 12, '2017-12-24', 320000, 'Chờ xử lý', 3, '2017-12-25', '10:00', '0988192713', 'Ngõ 11 Hà Trì 5, Hà Đông', ''),
(8, 12, '2017-12-24', 20000, 'Chờ xử lý', 1, '2017-12-25', '10:00', '0988192713', '112 Tô Hiệu, Cầu Giấy', 'Nóng'),
(9, 11, '2017-12-24', 32000, 'Chờ xử lý', 3, '2017-12-27', '10:00', '0988192713', 'Ngõ 11 Hà Trì 5, Hà Đông', ''),
(10, 12, '2017-12-24', 140000, 'Chờ xử lý', 1, '2017-12-25', '10:00', '0988192713', 'Ngõ 11 Hà Trì 5, Hà Đông', ''),
(11, 12, '2017-12-24', 128000, 'Chờ xử lý', 3, '2017-12-31', '11:30', '123', '72 Nguyễn Chí Thanh, Hà Nội', '72 NCT'),
(12, 12, '2017-12-24', 160000, 'Chờ xử lý', 1, '2017-12-28', '20:00', '12', '128 Ô chợ Dừa, Hà Nội', '123OCD'),
(13, 12, '2017-12-24', 120000, 'Đang giao hàng', 1, '2017-12-27', '11:30', '0988192713', '34 Phùng Hưng, Hà Nội', ''),
(14, 12, '2017-12-24', 120000, 'Chờ xử lý', 1, '2017-12-26', '10:00', '0988192713', 'Ngõ 11 Hà Trì 5, Hà Đông', ''),
(15, 13, '2018-01-02', 60000, 'Chờ xử lý', 1, '2018-01-08', '11:30', '123456789', 'Quận Hai Bà Trưng', ''),
(16, 13, '2018-01-03', 94000, 'Chờ xử lý', 6, '2018-01-09', '12:00', '0912345', 'Ngõ 11 Hà Trì 5, Hà Đông', 'Ngõ nằm cạnh số 283 Đường Tô Hiệu'),
(17, 13, '2018-01-03', 595000, 'Chờ xử lý', 4, '2018-01-07', '10:00', '123', '90 Bà Triệu, Hà Nội', ''),
(18, 13, '2018-01-03', 22000, 'Chờ xử lý', 5, '2018-01-10', '10:00', '0988192713', 'Ngõ 11 Hà Trì 5, Hà Đông', '');

-- --------------------------------------------------------

--
-- Table structure for table `monan_donhang`
--

DROP TABLE IF EXISTS `monan_donhang`;
CREATE TABLE IF NOT EXISTS `monan_donhang` (
  `maDonHang` int(11) NOT NULL,
  `maMonAn` int(11) NOT NULL,
  `soLuong` int(11) NOT NULL,
  PRIMARY KEY (`maDonHang`,`maMonAn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `monan_donhang`
--

INSERT INTO `monan_donhang` (`maDonHang`, `maMonAn`, `soLuong`) VALUES
(1, 1, 2),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(2, 1, 11),
(2, 3, 3),
(3, 1, 1),
(3, 2, 4),
(4, 1, 3),
(4, 2, 10),
(5, 3, 3),
(6, 2, 2),
(7, 1, 10),
(8, 4, 1),
(9, 1, 1),
(10, 2, 4),
(10, 4, 1),
(11, 1, 2),
(11, 2, 2),
(12, 1, 1),
(12, 2, 1),
(12, 3, 1),
(12, 4, 1),
(13, 1, 2),
(14, 1, 1),
(14, 4, 3),
(15, 2, 2),
(16, 2, 1),
(16, 4, 1),
(17, 1, 5),
(18, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

DROP TABLE IF EXISTS `taikhoan`;
CREATE TABLE IF NOT EXISTS `taikhoan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ten` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `matKhau` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `anhDaiDien` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  `facebookId` varchar(300) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`id`, `ten`, `email`, `matKhau`, `anhDaiDien`, `facebookId`) VALUES
(11, '', 'tungthai13@gmail.com', '', 'https://lh4.googleusercontent.com/-sj3SRygAVZs/AAAAAAAAAAI/AAAAAAAAAAA/AFiYof3LO63GR6S1FARZ1ptxuSCSdo6HXw/s96-c/photo.jpg', ''),
(12, 'Tùng Thái', '', '', 'http://graph.facebook.com/1894029894258699/picture?type=square', '1894029894258699'),
(13, '', 'boy.ps3@gmail.com', '', 'https://lh6.googleusercontent.com/-1xfhbzElrMM/AAAAAAAAAAI/AAAAAAAAAAA/AFiYof2dHZRRuhpZpUUtqPFhBvb91GFl2w/s96-c/photo.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `thucdon`
--

DROP TABLE IF EXISTS `thucdon`;
CREATE TABLE IF NOT EXISTS `thucdon` (
  `maCuaHang` int(11) NOT NULL,
  `maMonAn` int(11) NOT NULL,
  `tenMonAn` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `soLanDat` int(11) NOT NULL,
  `donGia` int(11) NOT NULL,
  `anhMinhHoa` varchar(300) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`maMonAn`,`maCuaHang`),
  KEY `maCuaHang` (`maCuaHang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `thucdon`
--

INSERT INTO `thucdon` (`maCuaHang`, `maMonAn`, `tenMonAn`, `soLanDat`, `donGia`, `anhMinhHoa`) VALUES
(1, 1, 'Suất cá', 6, 60000, 'motsuatca.jpg'),
(2, 1, 'Bánh Mì Thập Cẩm', 0, 32000, 'BanhMiThapCam.jpg'),
(3, 1, 'Hồng trà sữa', 27, 32000, 'hongtrasuadingtea.jpeg'),
(4, 1, 'Original Castella', 5, 119000, 'OriginalCastella.jpg'),
(5, 1, 'Lục Tào Xá', 0, 22000, 'LucTaoXa.jpg'),
(6, 1, 'Cơm tấm gà xào nấm', 0, 40000, 'ComGaSotNam.jpg'),
(7, 1, 'Trà Alisan kem sữa', 0, 47000, 'AlisanKemSua.jpg'),
(8, 1, 'Trà Xanh Heekcaa Cheese', 0, 47000, 'TraXanhHeekcaaCheese.jpg'),
(1, 2, 'Canh cá', 10, 30000, 'canhca.jpg'),
(2, 2, 'Bánh Mì Bò BBQ', 0, 32000, 'BanhMiBoNuong.jpg'),
(3, 2, 'Trà sữa ô long', 12, 32000, 'trasuaolongdingtea.jpeg'),
(4, 2, 'Chocolate Castella', 0, 149000, 'ChocolateCastella.jpg'),
(5, 2, 'Chí Mà Phù', 0, 22000, 'ChiMaPhu.jpg'),
(6, 2, 'Cơm tấm sườn nướng - chả trứng', 1, 45000, 'ComSuonBiCha.jpg'),
(7, 2, 'Trà xanh kem sữa', 0, 43000, 'TraXanhKemSua.jpg'),
(8, 2, 'Trà Ô Long Đào Cheese', 0, 45000, 'OLongDaoCheese.jpg'),
(1, 3, 'Suất lòng cá', 5, 50000, 'motsuatlong.jpg'),
(2, 3, 'Bánh Mì Trứng Xúc Xích', 0, 32000, 'BanhMiTrung.jpg'),
(4, 3, 'Cheese Castella', 0, 149000, 'CheeseCastella.jpg'),
(5, 3, 'Bánh Trôi Tàu', 1, 22000, 'BanhTroiTau.jpg'),
(6, 3, 'Cơm tấm cá kho tộ', 0, 49000, 'ComCaKhoTo.jpg'),
(1, 4, 'Khoai tây chiên', 6, 20000, 'khoaitaychien.jpg'),
(2, 4, 'Bánh Mì Gà Nướng', 0, 32000, 'BanhMiGaNuong.jpg'),
(4, 4, 'Pork Floss Castella', 0, 149000, 'PorkFlossCastella.jpg'),
(6, 4, 'Cơm bò sốt tiêu đen', 1, 49000, 'ComBoTieuDen.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_admin`
--

DROP TABLE IF EXISTS `user_admin`;
CREATE TABLE IF NOT EXISTS `user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `user_admin`
--

INSERT INTO `user_admin` (`id`, `email`, `password`) VALUES
(1, 'tung', '123');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `danhgiacuahang`
--
ALTER TABLE `danhgiacuahang`
  ADD CONSTRAINT `FK_MaCuaHangDanhGia` FOREIGN KEY (`maCuaHang`) REFERENCES `cuahang` (`maCuaHang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MaTaiKhoanDanhGia` FOREIGN KEY (`maTaiKhoan`) REFERENCES `taikhoan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thucdon`
--
ALTER TABLE `thucdon`
  ADD CONSTRAINT `thucdon_ibfk_1` FOREIGN KEY (`maCuaHang`) REFERENCES `cuahang` (`maCuaHang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
