<%-- 
    Document   : index
    Created on : Nov 6, 2017, 7:52:12 PM
    Author     : tungthai
--%>

<%@page import="java.sql.Connection"%>
<%@page import="utility.DBConnection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div id="all">

            <!--menu-->
            <%@include file="menu.jsp" %>

            <!--slide-->
            <%@include file="slide.jsp" %>

            <!--content-->
            <%@include file="content.jsp" %>

            <!--List user-->
            <%@include file="listuser.jsp" %>

            <!--Footer-->
            <%@include file="footer.jsp" %>

        </div>
    </body>
    
    <%@include file="scriptCuoiTrangChu.jsp" %>
</html>
