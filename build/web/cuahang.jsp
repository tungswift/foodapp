<%-- 
    Document   : index
    Created on : Nov 6, 2017, 7:52:12 PM
    Author     : tungthai
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="entity.CuaHang"%>
<%@page import="java.sql.Connection"%>
<%@page import="utility.DBConnection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<div id="content">
    <div class="container">
        <%
                String loaiCuaHang = String.valueOf(request.getAttribute("loaiCuaHang"));
                String loaiCuaHangStr = "";
                if(loaiCuaHang.equals("1")){
                    loaiCuaHangStr = "đồ uống";
                } else if(loaiCuaHang.equals("2")){
                    loaiCuaHangStr = "bánh";
                } else {
                    loaiCuaHangStr = "đồ ăn";
                }
        %>
        <!--Tab-->
        <div class="row select">
            <div class="col-md-4">
                <div class="tab">
                    <ul id="tabItem">
                        <li class="London active-slide" id='London1'>
                            <span class="w3-bar-item w3-button London" onclick="openCity('London')">Cửa hàng <%=loaiCuaHangStr%></span>
                        </li>
                        <li class="Tokyo" id='Tokyo1' style="display: none;">
                            <span class="w3-bar-item w3-button Tokyo" onclick="openCity('Tokyo')"> Đã Mua</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4"></div>

            <div class="col-md-4">
                <form class="form-inline" style="height: 49px;" action="CuaHang">
                    <input type="hidden" name="loaiCuaHang" value="<%=loaiCuaHang%>"/>
                    <label class="sr-only" for="inlineFormInput">Name</label>
                    <input type="text" name="timKiem" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Cửa hàng <%=loaiCuaHangStr%>">
                    <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                </form>
            </div>
        </div>
        <!--End tab-->

        <!--Tab 1 content-->
        <div class="row product w3-container w3-display-container city tabLondon" id="London">

            <%
                String timKiem = request.getParameter("timKiem");
                List<CuaHang> list = (List<CuaHang>) request.getAttribute("list");
                for (CuaHang cuaHang : list) {
                    int tongDiem = cuaHang.getTongDiem();
                    int soLuotCham = cuaHang.getSoLuotCham();
                    float diem;
                    if (soLuotCham == 0) {
                        diem = 0;
                    } else {
                        diem = (float) tongDiem / (float) soLuotCham;
                    }
            %>

            <!--Store Item-->
            <div class="col-md-4 default">
                <div class="item-product">
                    <div class="image">
                        <a href="#"><img src="image/<%=cuaHang.getLogo()%>" alt=""></a>

                    </div>
                    <div class="title">
                        <a href="#">
                            <div class="star">
                                <span><%=new DecimalFormat("#0.0").format(diem)%></span>
                            </div>
                            <div class="address">
                                <strong><%=cuaHang.getTenCuaHang()%></strong><br>
                                <span>
                                    <%=cuaHang.getDiaChi()%>
                                </span>
                            </div>

                        </a>
                    </div>
                    <div class="cach"></div>
                    <div class="order">
                        <form action="datmon" method="post" onsubmit="return kiemTraDangNhap();">
                            <input type="hidden" name="latHome" class="latHome" value="" />
                            <input type="hidden" name="lngHome" class="lngHome" value="" />
                            <input type="hidden" name="maCuaHang" value="<%=cuaHang.getMaCuaHang()%>" />
                            <input type="hidden" name="maTaiKhoan" class="maTaiKhoan" value="" />

                            <input type="submit" class="storeButton btn btn-submit" value="Đặt món"/>
                        </form>
                    </div>
                    <!--End button-->

                </div>
            </div>
            <!--End Store Item-->

            <% }%>

            <div class="container" style="text-align: center">
                <div class="pagination text-center" style="margin:0 auto;">
                    <%
                        String trangHienTai = String.valueOf(request.getAttribute("trangHienTai"));
                        String tongSoTrang = String.valueOf(request.getAttribute("tongSoTrang"));
                    %>
                    <input type="hidden" id="trangHienTai" value="<%=trangHienTai%>"/>

                    <a href="CuaHang?loaiCuaHang=<%=loaiCuaHang%>&trangHienTai=<%=Integer.parseInt(trangHienTai) - 1%>">&laquo;</a>
                    <%
                        for (int i = 1; i <= Integer.parseInt(tongSoTrang); i++) {
                    %>
                    <a id="trang<%=i%>" href="index.jsp?trangHienTai=<%=i%>"> <%=i%> </a>
                    <% }%>
                    <a href="CuaHang?loaiCuaHang=<%=loaiCuaHang%>&trangHienTai=<%=Integer.parseInt(trangHienTai) + 1%>">&raquo;</a>

                    <script>
                        activePhanTrang();
                    </script>
                </div>
            </div>
        </div>
        <!--End tab 1 content-->

        <!--Tab 3 content-->
        <div class="row product w3-container w3-display-container city" id="Tokyo" style="display:none">
            Tab 3
        </div>
        <!--End tab 3 content-->


    </div>
</div>