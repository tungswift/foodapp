<%-- 
    Document   : content
    Created on : Nov 6, 2017, 7:58:59 PM
    Author     : tungthai
--%>

<%@page import="dao.DonHangDAO"%>
<%@page import="entity.DonHang"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="dao.CuaHangDAO"%>
<%@page import="entity.CuaHang"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="content">
    <div class="container">

        <!--Tab-->
        <div class="row select">
            <div class="col-md-4">
                <div class="tab">
                    <ul id="tabItem">
                        <li class="London active-slide" id='London1' onclick="openCity('London')">
                            <span class="w3-bar-item w3-button London" >Cửa hàng</span>
                        </li>
                        <li class="Tokyo" id='Tokyo1' onclick="openCity('Tokyo')" hidden="true">
                            <span class="w3-bar-item w3-button Tokyo" > Đã Mua</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form class="form-inline" style="height: 49px;" action="index.jsp">
                    <label class="sr-only" for="inlineFormInput">Name</label>
                    <input type="text" name="timKiem" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Tìm kiếm cửa hàng">
                    <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                </form>
            </div>
        </div>
        <!--End tab-->

        <!--Tab 1 content-->
        <div class="row product w3-container w3-display-container city tabLondon" id="London">

            <%
                String timKiem = request.getParameter("timKiem");
                List<CuaHang> list;

                int tongSoCuaHang = new CuaHangDAO().tongSoCuaHang();
                int tongSoTrang = tongSoCuaHang / 9;
                if (tongSoTrang < 1) {
                    tongSoTrang = 1;
                } else if (tongSoTrang % 9 != 0) {
                    tongSoTrang += 1;
                }
                String trangHienTai = request.getParameter("trangHienTai");
                if (trangHienTai == null) {
                    trangHienTai = "1";
                } else {
                    if (Integer.parseInt(trangHienTai) <= 0) {
                        trangHienTai = "1";
                    }
                    if (Integer.parseInt(trangHienTai) > tongSoTrang) {
                        trangHienTai = tongSoTrang + "";
                    }
                }

                if (timKiem != null) {
                    list = new CuaHangDAO().timKiemCuaHang(timKiem);
                } else {
                    list = new CuaHangDAO().tatCaCuaHangPhanTrang(Integer.parseInt(trangHienTai), 9);
                }
                for (CuaHang cuaHang : list) {
                    int tongDiem = cuaHang.getTongDiem();
                    int soLuotCham = cuaHang.getSoLuotCham();
                    float diem;
                    if (soLuotCham == 0) {
                        diem = 0;
                    } else {
                        diem = (float) tongDiem / (float) soLuotCham;
                    }

            %>

            <!--Store Item-->
            <div class="col-md-4 default">
                <div class="item-product">
                    <div class="image">
                        <a href="#"><img src="image/<%=cuaHang.getLogo()%>" alt=""></a>

                    </div>
                    <div class="title">
                        <a href="#">
                            <div class="star">
                                <span><%=new DecimalFormat("#0.0").format(diem)%></span>
                            </div>
                            <div class="address">
                                <strong><%=cuaHang.getTenCuaHang()%></strong><br>
                                <span>
                                    <%=cuaHang.getDiaChi()%>
                                </span>
                            </div>

                        </a>
                    </div>
                    <div class="cach"></div>
                    <div class="order">
                        <form action="datmon" method="post" onsubmit="return kiemTraDangNhap();">
                            <input type="hidden" name="latHome" class="latHome" value="" />
                            <input type="hidden" name="lngHome" class="lngHome" value="" />
                            <input type="hidden" name="maCuaHang" value="<%=cuaHang.getMaCuaHang()%>" />
                            <input type="hidden" name="maTaiKhoan" class="maTaiKhoan" value="" />

                            <input type="submit" class="storeButton btn btn-submit" value="Đặt món"/>
                        </form>
                    </div>
                    <!--End button-->

                </div>
            </div>
            <!--End Store Item-->

            <% }%>
            <div class="container" style="text-align: center">
                <div class="pagination text-center" style="margin:0 auto;">
                    <input type="hidden" id="trangHienTai" value="<%=trangHienTai%>"/>

                    <a href="index.jsp?trangHienTai=<%=Integer.parseInt(trangHienTai) - 1%>">&laquo;</a>
                    <%
                        for (int i = 1; i <= tongSoTrang; i++) {
                    %>
                    <a id="trang<%=i%>" href="index.jsp?trangHienTai=<%=i%>"> <%=i%> </a>
                    <% }%>
                    <a href="index.jsp?trangHienTai=<%=Integer.parseInt(trangHienTai) + 1%>">&raquo;</a>

                    <script>
                        activePhanTrang();
                    </script>
                </div>
            </div>

        </div>

        <!--End tab 1 content-->

        <!--Tab 3 content-->
        <div class="row product w3-container w3-display-container city" id="Tokyo" style="display:none">

        </div>
        <!--End tab 3 content-->



    </div>
</div>
