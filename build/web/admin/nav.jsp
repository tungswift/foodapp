<%-- 
    Document   : nav
    Created on : Nov 8, 2017, 9:01:28 AM
    Author     : tungthai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String email=(String)session.getAttribute("email"); 
    if(email==null){
        response.sendRedirect("/FoodApp/admin/login/login.jsp");
    }
%>
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right"><img src="../../image/logo.png" alt=""></span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="../../image/avatar-admin.png" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
        <span>Hi! <strong><%=email%></strong></span><br> 
      <a href="logout.jsp" class="w3-bar-item w3-button"><i class="fa fa-cog"></i> <span> logout </span></a>
    </div>
  </div>
  <hr>
  <div class="w3-container">
    <h5>Quản Lí</h5>
  </div>
  <div class="w3-bar-block">
    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Đóng</a>
    <a href="/FoodApp/admin/login/" class="w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i> Cửa Hàng</a>
    <a href="/FoodApp/admin/menu" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i> Thực Đơn</a> 
    <a href="/FoodApp/admin/donhang" class="w3-bar-item w3-button w3-padding"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> Đơn Hàng</a> 
  
  </div>
</nav>
